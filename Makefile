FILES = \
	EML.eml \
	HTML.html MHTML.mhtml XML.xml \
	MS-DOC.doc MS-DOCM.docm MS-DOCX.docx MS-DOT.dot \
	MS-POT.pot MS-PPTM.pptm MS-PPT.ppt MS-PPTX.pptx \
	MS-XLSM.xlsm MS-XLS.xls MS-XLSX.xlsx MS-XLT.xlt \
	OPEN-ODP.odp OPEN-ODS.ods OPEN-ODT.odt \
	PDF.pdf OFD.ofd \
	RTF.rtf TXT.txt \
	UOF-UOP.uop UOF-UOS.uos UOF-UOT.uot \
	WPS-DPS.dps WPS-DPT.dpt WPS-ET.et WPS-ETT.ett WPS-WPS.wps WPS-WPT.wpt \
	YOZO-EID.eid YOZO-EIP.eip YOZO-EIS.eis
TESTFILES = $(addprefix test_file/, $(FILES))
OUTFILES = $(addprefix out_file/, $(addsuffix .out, $(FILES)))

all: $(OUTFILES)

out_file/%.out: test_file/%
	# $^
	-@LD_LIBRARY_PATH=lib ./bin/doctotext $^>$@; \
		if [ $$? -ne 0 ]; then \
			$(RM) $@; \
			echo $^ NO>>result; \
		else \
			echo $^ YES>>result; \
			cat $@; \
		fi

clean:
	$(RM) out_file/* result

.PHONY: all clean

